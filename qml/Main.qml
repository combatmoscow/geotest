import QtQuick 2.0
import QtPositioning 5.11
import Kombat.com 1.0

App {
  id: app
  property string port: "5055"
  property int counter: 0


  Server {
      id: server
      onOpenPortCompleted: {
          if(success) {
              stateText.text = " Server is running "
          } else {
              stateText.text = " Unable to start the server "
          }
      }
      onConnectionEnable: {
          if(success) {
              ipStatusBox.color = "green"
          } else {
              ipStatusBox.color = "red"
          }
      }
      onSendDebug: {
        sendingDataText.text = sendingText
      }
  }

  PositionSource {
      id: src
      active: true
      updateInterval: 1000
      onPositionChanged: {
          counter++
          var coord = src.position.coordinate
          ltText.text = "lt=" + coord.latitude + ", "
          lnText.text = "ln=" + coord.longitude
          server.sendGeo(src.position.coordinate)
      }
  }

  NavigationStack {
    Page {
      id: mainPage
      title: "KOMBAT GPS"

      Rectangle {
          anchors.top: parent.top
          anchors.topMargin: dp(20)
          anchors.horizontalCenter: parent.horizontalCenter
          width: dp(20)
          height: dp(20)
          color: src.sourceError === PositionSource.NoError ? "#4f8901" : "#dc0f0f"
          radius: 15
      }

      Rectangle {
          id: ipStatusBox
          anchors.top: parent.top
          anchors.topMargin: dp(50)
          anchors.horizontalCenter: parent.horizontalCenter
          width: dp(30)
          height: dp(30)
          color: "red"
          border.color: "black"
          radius: 15
      }
      Column {
        anchors.centerIn: parent
        spacing: 5

        AppText {
          anchors.horizontalCenter: parent.horizontalCenter
          text: "Counter " + app.counter
        }
        AppText {
          id: stateText
          anchors.horizontalCenter: parent.horizontalCenter
          text: "Server is off"
        }
        AppText {
          anchors.horizontalCenter: parent.horizontalCenter
          text: app.port
          MouseArea {
              anchors.fill: parent
              onClicked: {
                  mainPage.navigationStack.push(portPageComponent)
              }
          }
        }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            AppButton {
              text: "Open port"
              radius: 15
              borderColor: "white"
              borderWidth: 1
              onClicked: {
                  console.log(" port = " + app.port)
                  server.openPort(parseInt(app.port))
              }
            }
            AppButton {
              text: "Close port"
              radius: 15
              borderColor: "white"
              borderWidth: 1
              onClicked: {
                  server.closeServer()
                  stateText.text = "Server is off"
              }
            }
        }
        Row {
            anchors.horizontalCenter: parent.horizontalCenter
            AppText {
              id: ltText
              text: "lt=0, "
            }
            AppText {
              id: lnText
              text: "ln=0 "
            }
        }
        AppText {
            id: sendingDataText
          anchors.horizontalCenter: parent.horizontalCenter
          text: "sending data"
        }
      }

      Component.onCompleted: {
          console.log(" port = " + app.port)
          server.openPort(parseInt(app.port))
      }
    }
  }

//  Component {
//    id: portPageComponent
//    Page {
//      title: "Choose port"
//      property Page target: null
//      AppListView {
//          delegate: SimpleRow {
//              onSelected: {
//                  console.log(" text = " + text)
//                  app.port = text
//                  mainPage.navigationStack.pop()
//              }
//          }
//          model: [
//              { text: "5055" },
//              { text: "5056" },
//              { text: "5057" },
//              { text: "5058" },
//              { text: "5059" },
//              { text: "5060" }
//          ]
//      }
//    }
//  }
}
